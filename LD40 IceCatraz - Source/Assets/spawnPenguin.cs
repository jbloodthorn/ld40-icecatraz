﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnPenguin : MonoBehaviour {

	public float timeBetweenSpawnChecks = 1f;

	public penguin[] spawnPrefabs;
	public GameObject spawnParent; 
	public GameObject scoreManager;
	ScoreManager scoreScript;

	private float nextSpawnCheck = 0.0f;
	private float spawnChance;

	// Use this for initialization
	void Start () {
		scoreScript = scoreManager.GetComponent <ScoreManager> ();
	}

	// Update is called once per frame
	void Update () {
		if (Time.time > nextSpawnCheck) {
			nextSpawnCheck = Time.time + timeBetweenSpawnChecks;
			float spawnRoll = Random.Range(0f, 100.0f);
			int numberOfPenguins = Mathf.Clamp(scoreScript.GetNumberOfPenguins () - 5, 0, 20);

			spawnChance = (0.2404491735f * Mathf.Log (numberOfPenguins) + 0.1130119842f) * 100 + 10;

//			Debug.Log ("Time to check for penguin spawn. Rolled " + spawnRoll + " vs " + spawnChance + " (base " + spawnChance + ")");
			if (spawnRoll <= spawnChance && numberOfPenguins > 0) {
//				Debug.Log ("Spawning a penguin.");
				SpawnThing ();
				scoreScript.EscapePenguin ();
			}
		}		
	}

	void SpawnThing () {
		penguin nextPenguin = spawnPrefabs [scoreScript.securityLevel];
		penguin newPenguin = Instantiate <penguin> (nextPenguin);

		newPenguin.transform.localPosition = transform.position;
		newPenguin.transform.parent = spawnParent.transform;
		newPenguin.health += scoreScript.securityLevel;
	}
}
