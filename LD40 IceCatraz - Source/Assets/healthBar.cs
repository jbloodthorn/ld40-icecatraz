﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class healthBar : MonoBehaviour {

	float maxHealth;
	float curHealth;
	float maxSize;

	// Use this for initialization
	void Start () {
		maxHealth = this.GetComponentInParent<penguin> ().health;
		maxSize = this.transform.localScale.x;
	}
	
	// Update is called once per frame
	void Update () {
		curHealth = this.GetComponentInParent<penguin> ().health;
		float healthPercent = curHealth / maxHealth;
		this.GetComponent<SpriteRenderer> ().color = Color.Lerp (Color.red, Color.green, healthPercent);
		float newSize = healthPercent * maxSize; 
		this.transform.localScale = new Vector3 (newSize, .3f, 1f);
	}
}
