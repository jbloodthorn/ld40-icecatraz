﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class leopardseal : MonoBehaviour {

	public float speed = .25f;
	public int health = 1;

	GameObject pathToFollow;
	GameObject scoreManager;
	ScoreManager scoreScript;

	Transform targetWaypoint;
	int WaypointIndex = 0;

	// Use this for initialization
	void Start () {
		pathToFollow = GameObject.Find ("PathLeopardSeal");
		scoreManager = GameObject.Find ("ScoreManager");
		scoreScript = scoreManager.GetComponent <ScoreManager> ();
	}

	// Update is called once per frame
	void Update () {
		// Move to next waypoint
		if (targetWaypoint == null) { 
			GetNextWaypoint ();
			if (targetWaypoint == null) {
				ReachedGoal ();
			}
		}

		Vector2 dir = targetWaypoint.position - this.transform.localPosition;

		float distThisFrame = speed * Time.deltaTime;

		if (dir.magnitude <= distThisFrame) {
			targetWaypoint = null;
		} else {
			transform.Translate ( dir.normalized * distThisFrame, Space.World );
		}
	}

	void OnMouseDown () {
		Debug.Log ("Leopard Seal clicked.");
		health--;
		if (health <= 0) {
			scoreScript.DestroySeal ();
			Destroy (gameObject);
		}		
	}

	void GetNextWaypoint () {
		targetWaypoint = pathToFollow.transform.GetChild (WaypointIndex);
		WaypointIndex++;
	}

	void ReachedGoal () {
		scoreScript.LoseLife (2);
		Destroy (gameObject);
	}

	void OnTriggerEnter2D(Collider2D other) {
		ReachedGoal ();
	}
}
