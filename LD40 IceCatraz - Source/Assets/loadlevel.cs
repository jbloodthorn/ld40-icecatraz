﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class loadlevel : MonoBehaviour {

	public void LoadLevel () {
		SceneManager.LoadScene ("MainScene");
	}

	public void LoadMenu () {
		SceneManager.LoadScene ("Menu");
	}
}
