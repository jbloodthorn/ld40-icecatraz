﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnLeopardSeal : MonoBehaviour {

	public float timeBetweenSpawnChecks = 1f;

	public leopardseal[] spawnPrefabs;
	public GameObject spawnParent; 
	public GameObject scoreManager;
	ScoreManager scoreScript;

	private float nextSpawnCheck = 0.0f;
	private float spawnChance;

	// Use this for initialization
	void Start () {
		scoreScript = scoreManager.GetComponent <ScoreManager> ();
	}

	// Update is called once per frame
	void Update () {
		if (Time.time > nextSpawnCheck) {
			nextSpawnCheck = Time.time + timeBetweenSpawnChecks;
			float spawnRoll = Random.Range(0f, 100.0f);
			int numberOfSeals = Mathf.Clamp(scoreScript.GetNumberOfPenguins () - 5, 0, 20);

			spawnChance = (0.2404491735f * Mathf.Log (numberOfSeals) + 0.1130119842f) * 100 + 10;

			//			Debug.Log ("Time to check for penguin spawn. Rolled " + spawnRoll + " vs " + spawnChance + " (base " + spawnChance + ")");
			if (spawnRoll <= spawnChance && numberOfSeals > 0) {
				//				Debug.Log ("Spawning a penguin.");
				SpawnThing ();
				scoreScript.IncomingSeal ();
			}
		}		
	}

	void SpawnThing () {
		leopardseal nextLeopardSeal = spawnPrefabs [scoreScript.securityLevel];
		leopardseal newLeopardSeal = Instantiate <leopardseal> (nextLeopardSeal);

		newLeopardSeal.transform.localPosition = transform.position;
		newLeopardSeal.transform.parent = spawnParent.transform;
		newLeopardSeal.health += (scoreScript.securityLevel * 2);
	}
}
