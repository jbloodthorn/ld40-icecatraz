﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {

	public int lives = 20;
	public float munny = 0f;
	public float score = 0;

	public int securityLevel = 0;

	public int[] penguinCount;
	private int[] capturedPenguins = new int[5];
	private int[] defeatedSeals = new int[5];

	public GameObject GOPanel;
	public Text MunnyLabel;
	public Text LivesLabel;
	public Text messageOut;
	public Text[] Labels;
	public Text[] GameOverPenguinCounts;
	public Text[] GameOverSealCounts;
	public Text FinalScore;


	public float messageLiveTime = 3f;

	private bool netNeutrality = true;
	private bool doneScoring = false;

	private float nextMessageBlank = 0.0f;
	private float delayBeforeShowingScore = 1f;
	private float timeToDisplayScore = 0.0f;


	// Use this for initialization
	void Start () {
		penguinCount [0] = 0;
		penguinCount [1] = 0;
		penguinCount [2] = 0;
		penguinCount [3] = 0;
		penguinCount [4] = 0;
	}

	// Update is called once per frame
	void Update () {
		// points can be partial, if there is a bonus % or something. Maybe.
		int currentMunny = (int) munny;
		MunnyLabel.text = "Munny: " + currentMunny.ToString();

		LivesLabel.text = "Lives: " + lives.ToString();

		Labels[0].text = "Jailbirds: " + penguinCount[0];
		Labels[1].text = "Beakmailers: " + penguinCount[1];
		Labels[2].text = "Shellcrackers: " + penguinCount [2];
		Labels[3].text = "Maleflippers: " + penguinCount[3];
		Labels[4].text = "Kingpinguins: " + penguinCount[4];

		if (Time.time >= nextMessageBlank) {
			messageOut.text = "";
		}

		if (timeToDisplayScore > 0 && Time.time >= timeToDisplayScore && !doneScoring) {
			doneScoring = true;
			GOPanel.SetActive (true);

			for(int xx = 0; xx < capturedPenguins.Length; xx++) {
				GameOverPenguinCounts [xx].text = "Level " + (xx + 1) + ": " + capturedPenguins [xx];
				score += capturedPenguins [xx] * (xx + 1);
			}
			for(int xx = 0; xx < defeatedSeals.Length; xx++) {
				GameOverSealCounts [xx].text = "Level " + (xx + 1) + ": " + defeatedSeals [xx];
				score += defeatedSeals [xx] * (xx + 1) * 5;
			}

			FinalScore.text = score.ToString ();
		}

		if (netNeutrality == false) {
			AddPenguin ();
		}
	}

	public void LoseLife (int numToLose = 1) {
		lives -= numToLose;
		if (lives <= 0) {
			GameOverMan ();
		}
	}

	public void GameOverMan () {
		Debug.Log ("GameOver");
		netNeutrality = true;
		messageOut.text = "Game Over, Man!";
		nextMessageBlank = Time.time + messageLiveTime;
		timeToDisplayScore = Time.time + delayBeforeShowingScore;
		GameObject.Find ("SpawnManager").SetActive (false);
	}

	public void GetPoints (int munnyToAdd) {
		Debug.Log ("Got munny: " + munnyToAdd);
		munny += munnyToAdd;
	}

	public void AddPenguin () {
		penguinCount [Random.Range (0, securityLevel + 1)]++;
	}

	public void BuyPenguin () {
		if (munny >= 10f) {
			penguinCount [securityLevel]++;
			munny -= 10f;
		} else {
			// have some output on screen here
		}
	}

	public void EscapePenguin () {
		penguinCount [securityLevel]--;
	}

	public void RecoverPenguin () {
		penguinCount [securityLevel]++;
		// Penguins already on the field when security level is upgraded will count towards the new higher level
		// It's a bonus. Yep. >_>
		capturedPenguins[securityLevel] += (securityLevel + 1);
	}

	public void IncomingSeal () {
		
	}

	public void DestroySeal () {
		GetPoints (50 * (securityLevel + 1));
		defeatedSeals [securityLevel]++;
	}

	public int GetNumberOfPenguins () {
		return Mathf.Clamp (penguinCount [securityLevel], 0, 40);
	}

	public void IncreaseSecurity () {
		if(munny >= 100f) {
			securityLevel++;
			Labels [securityLevel].color = Color.black;
			munny -= 100f;
		}
	}

	public void EndNetNeutrality () {
		if (munny <= 1000f) {
			Debug.Log ("Ending net neutrality");
			netNeutrality = false;
			messageOut.text = "Activists take to streets!\nMartial law declared!";
			nextMessageBlank = Time.time + messageLiveTime;
			GameObject.Find ("FCCButton").GetComponent <Button>().interactable = false;
		}
	}
}
