﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class penguin : MonoBehaviour {

	public float speed = 5f;
	public int health = 1;

	GameObject scoreManager;
	GameObject pathGO;
	ScoreManager scoreScript;

	Transform targetPathNode;
	int pathNodeIndex = 0;

	// Use this for initialization
	void Start () {
		pathGO = GameObject.Find ("PathPenguin");
		scoreManager = GameObject.Find ("ScoreManager");
		scoreScript = scoreManager.GetComponent <ScoreManager> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (targetPathNode == null) {
			GetNextPathNode ();
			if (targetPathNode == null) {
				ReachedGoal ();
			}
		}

		Vector2 dir = targetPathNode.position - this.transform.localPosition;

		float distThisFrame = speed * Time.deltaTime;

		if (dir.magnitude <= distThisFrame) {
			targetPathNode = null;
		} else {
			transform.Translate ( dir.normalized * distThisFrame, Space.World );
		}

	}

	void OnMouseDown () {
		Debug.Log ("Penguin clicked.");
		health--;
		if (health <= 0) {
			scoreScript.RecoverPenguin ();
			Destroy (gameObject);
		}		
	}

	void GetNextPathNode () {
		targetPathNode = pathGO.transform.GetChild (pathNodeIndex);
		pathNodeIndex++;

	}

	void ReachedGoal () {		
		Debug.Log ("Lose a life");
		scoreManager.GetComponent<ScoreManager>().LoseLife ();
		Destroy (gameObject);
	}

	void OnTriggerEnter2D(Collider2D enteredCollider) {
		if (enteredCollider.CompareTag ("PenguinGoal")) {
			ReachedGoal ();
		}
	}


}
